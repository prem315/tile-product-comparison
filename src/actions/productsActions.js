import * as _ from "lodash";
import { getProductsAPI } from "../apis/productsAPIs";

export const REQUEST_PRODUCTS = "REQUEST_RESELLER_ACTIVITY";
export const REQUEST_PRODUCTS_FAILED = "REQUEST_PRODUCTS_FAILED";
export const REQUEST_PRODUCTS_SUCCESS = "REQUEST_PRODUCTS_SUCCESS";

export const FEATURES_LIST = "FEATURES_LIST";

export const getProducts = (data) => async (dispatch) => {
  dispatch({ type: REQUEST_PRODUCTS });
  return getProductsAPI(data)
    .then((result) => {
      const images = result.data.products.compareSummary.images;
      const productPricingSummary =
        result.data.products.compareSummary.productPricingSummary;
      const titles = result.data.products.compareSummary.titles;

      const products = [];

      _.forIn(images, (value, key) => {
        let productObj = {};
        productObj["productid"] = key;
        productObj["img"] = value;
        products.push(productObj);
      });

      _.forIn(productPricingSummary, (value, key) => {
        products.map((data) => {
          if (data.productid === key) {
            data["productPricing"] = value;
          }
        });
      });

      _.forIn(titles, (value, key) => {
        products.map((data) => {
          if (data.productid === key) {
            data["titles"] = value;
          }
        });
      });

      const featureList = result.data.products.featuresList;

      const featureSize = Object.keys(featureList[0].features[0].values).map(
        (key) => ({
          key,
          size: featureList[0].features[0].values[key],
        })
      );

      //   console.log(arr);

      //_forIn(featureList[0].features[0])

      dispatch({
        type: REQUEST_PRODUCTS_SUCCESS,
        products: products,
      });

      dispatch({
        type: FEATURES_LIST,
        featureList: featureList,
      });
    })
    .catch((error) => {
      if (error.response.status === 500) {
        dispatch({
          type: REQUEST_PRODUCTS_FAILED,
          error: true,
        });
      }
    });
};
