export const ADD_PRODUCT = "ADD_PRODUCT";
export const ADD_PRODUCT_FAILED = "ADD_PRODUCT_FAILED";
export const ADD_PRODUCT_SUCCESS = "ADD_PRODUCT_SUCCESS";

export const REMOVE_PRODUCT = "REMOVE_PRODUCT";

export const addProductToList = (product) => {
  return {
    type: ADD_PRODUCT,
    product: product,
  };
};

export const removeProductFromList = (product) => {
  return {
    type: REMOVE_PRODUCT,
    product: product,
  };
};
