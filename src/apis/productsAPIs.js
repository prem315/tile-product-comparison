import axios from "axios";
import * as _ from "lodash";

export function getProductsAPI() {
  return new Promise((res, rej) =>
    axios
      .get(`http://www.mocky.io/v2/5e9ebdaa2d00007800cb7697`)
      .then((response) => {
        res(response);
      })
      .catch((error) => {
        rej(error);
      })
  );
}
