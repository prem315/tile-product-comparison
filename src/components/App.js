import { Route, Switch } from "react-router-dom";
import NotFoundPage from "./NotFoundPage";
import PropTypes from "prop-types";
import React from "react";
import { hot } from "react-hot-loader";
import Products from "./containers/Products/Products";
import CompareProducts from "./containers/CompareProducts/CompareProducts";

class App extends React.Component {
  render() {
    return (
      <div className="pageBody">
        <Switch>
          <Route exact path="/" component={Products} />
          <Route path="/compare" component={CompareProducts} />

          <Route component={NotFoundPage} />
        </Switch>
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.element,
};

export default hot(module)(App);
