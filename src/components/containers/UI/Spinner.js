import React from "react";

const Spinner = () => {
  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <div className="spinner-grow" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    </div>
  );
};

export default Spinner;

// inside
