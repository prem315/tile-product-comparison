import React from "react";

const AlertBar = ({ variant, alertMsg }) => {
  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <div className={`alert alert-${variant}`} role="alert">
        {alertMsg}
      </div>
    </div>
  );
};

export default AlertBar;

// inside
