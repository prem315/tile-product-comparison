import React from "react";

const CompareTable = ({
  featureTitle,
  displayFeaturesNames,
  productsList,
  compareData,
}) => {
  return (
    <>
      <div className="row no-gutters">
        <div className="col-md-3">
          <h5
            style={{
              backgroundColor: "#dee2e6",
              paddingTop: "5px",
              paddingBottom: "5px",
              paddingLeft: "6px",
              marginBottom: "0px",
            }}
          >
            {featureTitle}
          </h5>
        </div>
        <div className="col-md-9">
          <div className="row no-gutters">
            {productsList.compareData.map((data, idx) => {
              return (
                <div
                  className="col-md-3"
                  style={{ height: "34px ", backgroundColor: "#dee2e6" }}
                  key={idx}
                >
                  <h5
                    style={{
                      backgroundColor: "#dee2e6",
                      paddingTop: "5px",
                      paddingBottom: "5px",
                      paddingLeft: "6px",
                      marginBottom: "0px",
                    }}
                  ></h5>
                </div>
              );
            })}
            {compareData.length < 4 ? (
              <div
                className="col-md-3"
                style={{ height: "34px ", backgroundColor: "#dee2e6" }}
              >
                <h5
                  style={{
                    backgroundColor: "#dee2e6",
                    paddingTop: "5px",
                    paddingBottom: "5px",
                    paddingLeft: "6px",
                    marginBottom: "0px",
                  }}
                ></h5>
              </div>
            ) : null}
          </div>
        </div>
      </div>
      <div className="row no-gutters">
        <div
          className="col-md-3 "
          style={{
            borderRight: "1px solid #e8e8e8",
            padding: "0px !important",
          }}
        >
          {displayFeaturesNames.features.map((feature, idx) => {
            return (
              <div
                style={{
                  height: "40px",
                  borderBottom: "1px solid #e8e8e8",
                }}
                key={idx}
              >
                <p
                  style={{
                    paddingTop: "9px",
                    fontSize: "0.8rem",
                    paddingLeft: "5px",
                    fontWeight: "500",
                  }}
                >
                  {feature.featureName}
                </p>
              </div>
            );
          })}
        </div>
        <div className="col-md-9">
          <div className="row no-gutters">
            {productsList.compareData.map((data) => {
              const d = data.find((d) => {
                return d.title === featureTitle;
              });
              const features = d.features;
              return (
                <div
                  className="col-md-3"
                  style={{
                    borderRight: "1px solid #e8e8e8",
                  }}
                  key={d.productid}
                >
                  <div>
                    {features.map((feature, idx) => {
                      return (
                        <div
                          key={idx}
                          style={{
                            height: "40px",
                            borderBottom: "1px solid #e8e8e8",
                          }}
                        >
                          <p
                            style={{
                              paddingTop: "9px",
                              fontSize: "0.8rem",
                              paddingLeft: "5px",
                            }}
                          >
                            {feature.value}
                          </p>
                        </div>
                      );
                    })}
                  </div>
                </div>
              );
            })}

            {compareData.length < 4 ? (
              <div
                className="col-md-3"
                style={{
                  borderRight: "1px solid #e8e8e8",
                }}
              >
                <div>
                  {displayFeaturesNames.features.map((feature, idx) => {
                    return (
                      <div
                        key={idx}
                        style={{
                          height: "40px",
                          borderBottom: "1px solid #e8e8e8",
                        }}
                      >
                        <p
                          style={{
                            paddingTop: "9px",
                            fontSize: "0.8rem",
                            paddingLeft: "5px",
                          }}
                        ></p>
                      </div>
                    );
                  })}
                </div>
              </div>
            ) : null}
          </div>
        </div>
      </div>
    </>
  );
};

export default CompareTable;
