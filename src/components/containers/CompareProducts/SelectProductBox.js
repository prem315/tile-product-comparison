import React from "react";

const SelectProductBox = ({
  availableProductsForComparision,
  handleChangeProduct,
}) => {
  return (
    <div className="productCard">
      <div style={{ position: "relative" }}>
        <img
          src="https://fakeimg.pl/300/"
          style={{ width: "100%", height: "150px" }}
          alt="..."
        />
        <p style={{ fontWeight: "500", fontSize: "0.8rem", marginTop: "10px" }}>
          Add Product
        </p>
        <select
          onChange={handleChangeProduct}
          style={{
            outline: "0",
            borderWidth: "0 0 2px",
            borderColor: "#dee2e6",
            width: "180px",
          }}
        >
          <option value={null}>select</option>
          {availableProductsForComparision.map((product) => (
            <option key={product.productid} value={product.productid}>
              {product.titles.title}
            </option>
          ))}
          ;
        </select>
      </div>
    </div>
  );
};

export default SelectProductBox;
