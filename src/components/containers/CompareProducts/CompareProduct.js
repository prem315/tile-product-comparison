import React from "react";
import deleteIcon from "../../../images/cross.svg";

const CompareProduct = ({
  product,
  productList,
  removeProductFromComparision,
}) => {
  return (
    <div className="col-md-3">
      <div className="productCard">
        <div style={{ position: "relative" }}>
          <img
            src={product.img}
            style={{ width: "100%", height: "150px" }}
            alt="..."
          />

          {productList.length > 1 ? (
            <div
              onClick={() => removeProductFromComparision(product)}
              style={{
                position: "absolute",
                right: "-8px",
                top: "-10px",
                backgroundColor: "#fff",
                height: "20px",
                width: "20px",
                borderRadius: "50%",
                border: "1px solid #e6e6e6",
                boxShadow:
                  "0 10px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)",
                display: "flex",
                justifyContent: "center",
                cursor: "pointer",
              }}
            >
              <img src={deleteIcon} style={{ width: "13px", width: "8px" }} />
            </div>
          ) : null}
        </div>
        <p style={{ fontSize: "0.8rem", marginTop: "10px" }}>
          {product.titles.title}
        </p>
        <p style={{ display: "flex", justifyContent: "space-between" }}>
          <span style={{ fontWeight: "500", fontSize: "0.8rem" }}>
            &#8377;{product.productPricing.finalPrice}
          </span>
          <span
            style={{
              fontWeight: "500",
              fontSize: "0.8rem",
              textDecoration: "line-through",
              color: "#b9b6b6",
            }}
          >
            &#8377;{product.productPricing.price}
          </span>
          <span
            style={{ fontWeight: "500", fontSize: "0.8rem", color: "#4CAF50" }}
          >
            {product.productPricing.totalDiscount} %
          </span>
        </p>
      </div>
    </div>
  );
};

export default CompareProduct;
