import React from "react";
import { connect } from "react-redux";
import _ from "lodash";
import {
  addProductToList,
  removeProductFromList,
} from "../../../actions/productsComparisionActions";
import { getProducts } from "../../../actions/productsActions";
import CompareProduct from "./CompareProduct";
import Navbar from "../Navbar/Navbar";
import Spinner from "../UI/Spinner";
import SelectProductBox from "./SelectProductBox";
import CompareTable from "./CompareTable";

class CompareProducts extends React.Component {
  constructor(props) {
    super(props);
    this.handleChangeProduct = this.handleChangeProduct.bind(this);
  }
  async componentDidMount() {
    if (this.props.location.data !== undefined) {
      let { product } = await this.props.location.data;
      await this.props.addProductToList(product);
    } else {
      await this.props.getProducts();
      let productFromList = await this.props.products.products.find(
        (product) => product.productid === "TVSF2WYUE4PWNJKM"
      );
      await this.props.addProductToList(productFromList);
    }
  }

  handleChangeProduct(event) {
    let product = this.props.products.products.find(
      (product) => product.productid === event.target.value
    );

    this.props.addProductToList(product);
  }

  removeProductFromComparision = (product) => {
    this.props.removeProductFromList(product);
  };
  render() {
    let availableProductsForComparision = this.props.products.products.filter(
      (product) => !this.props.productsList.productList.includes(product)
    );

    const displayFeaturesNames = _.find(
      this.props.productsList.compareData[0],
      {
        title: "Display",
      }
    );

    const GeneralFeaturesNames = _.find(
      this.props.productsList.compareData[0],
      {
        title: "General Features",
      }
    );

    const internetFeaturesNames = _.find(
      this.props.productsList.compareData[0],
      {
        title: "Internet Features",
      }
    );

    if (
      displayFeaturesNames === undefined ||
      GeneralFeaturesNames === undefined ||
      internetFeaturesNames === undefined
    ) {
      return (
        <>
          <Navbar />
          <Spinner />
        </>
      );
    }
    if (displayFeaturesNames) {
      return (
        <>
          <Navbar />
          <div className="container">
            <div className="row">
              <div className="col-md-3"></div>
              <div className="col-md-9">
                <div className="row">
                  {this.props.productsList.productList.map((product) => {
                    return (
                      <CompareProduct
                        product={product}
                        productList={this.props.productsList.productList}
                        removeProductFromComparision={
                          this.removeProductFromComparision
                        }
                        key={product.productid}
                      />
                    );
                  })}
                  <div className="col-md-3">
                    {availableProductsForComparision.length === 0 &&
                    this.props.productsList.productList.length <= 4 ? null : (
                      <SelectProductBox
                        availableProductsForComparision={
                          availableProductsForComparision
                        }
                        handleChangeProduct={this.handleChangeProduct}
                      />
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* display  */}

          <div className="container">
            <CompareTable
              featureTitle="Display"
              displayFeaturesNames={displayFeaturesNames}
              productsList={this.props.productsList}
              compareData={this.props.productsList.compareData}
            />

            <CompareTable
              featureTitle="General Features"
              displayFeaturesNames={GeneralFeaturesNames}
              productsList={this.props.productsList}
              compareData={this.props.productsList.compareData}
            />

            <CompareTable
              featureTitle="Internet Features"
              displayFeaturesNames={internetFeaturesNames}
              productsList={this.props.productsList}
              compareData={this.props.productsList.compareData}
            />
          </div>
        </>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.productsReducer,
    productsList: state.productsComparisonReducer,
  };
};

export default connect(mapStateToProps, {
  addProductToList,
  removeProductFromList,
  getProducts,
})(CompareProducts);
