import React from "react";
import { Link } from "react-router-dom";

const Product = (props) => {
  return (
    <div className="col-md-3">
      <div className="productCard">
        <div style={{ position: "relative" }}>
          <img
            src={props.product.img}
            style={{ width: "100%", height: "200px" }}
            alt="..."
          />
        </div>
        <p style={{ fontSize: "0.8rem", marginTop: "10px" }}>
          {props.product.titles.title}
        </p>
        <p style={{ display: "flex", justifyContent: "space-between" }}>
          <span style={{ fontWeight: "500", fontSize: "0.8rem" }}>
            &#8377;{props.product.productPricing.finalPrice}
          </span>
          <span
            style={{
              fontWeight: "500",
              fontSize: "0.8rem",
              textDecoration: "line-through",
              color: "#b9b6b6",
            }}
          >
            &#8377;{props.product.productPricing.price}
          </span>
          <span
            style={{ fontWeight: "500", fontSize: "0.8rem", color: "#4CAF50" }}
          >
            {props.product.productPricing.totalDiscount} %
          </span>
        </p>

        <Link
          to={{
            pathname: "/compare",
            data: {
              product: props.product,
            },
          }}
          style={{
            height: "200px",
            padding: "10px",
            border: "1px solid black",
            color: "black",
          }}
        >
          compare product
        </Link>
      </div>
    </div>
  );
};

// const Product = (props) => {
//   return (
//     <div className="col-md-3">
//       <div className="card">
//         <img src={props.product.img} className="card-img-top" alt="..." />
//         <div className="card-body">
//           <h5 className="card-title">{props.product.titles.title}</h5>

//           <Link
//             to={{
//               pathname: "/compare",
//               data: {
//                 product: props.product,
//               },
//             }}
//             className="btn btn-primary"
//           >
//             compare product
//           </Link>
//         </div>
//       </div>
//     </div>
//   );
// };

export default Product;

// inside products
