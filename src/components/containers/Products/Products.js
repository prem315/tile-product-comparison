import React from "react";
import { connect } from "react-redux";
import { getProducts } from "../../../actions/productsActions";
import Navbar from "../Navbar/Navbar";
import Product from "./Product";
import Spinner from "../UI/Spinner";
import AlertBar from "../UI/AlertBar";

class Products extends React.Component {
  componentDidMount() {
    this.props.getProducts();
  }
  render() {
    const { loading, error } = this.props.products;

    return (
      <>
        <Navbar />
        {loading === true ? <Spinner /> : null}
        {error === true ? (
          <AlertBar
            variant="danger"
            alertMsg="Something went wrong. Please try again."
          />
        ) : null}
        <div className="container">
          <div className="row">
            {this.props.products.products.map((product) => {
              return <Product product={product} key={product.productid} />;
            })}
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.productsReducer,
  };
};

export default connect(mapStateToProps, { getProducts })(Products);
//  inside products folder
