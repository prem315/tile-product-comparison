import * as actionTypes from "../actions/productsActions";
import update from "immutability-helper";

const initialState = {
  products: [],
  loading: false,
  error: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case actionTypes.REQUEST_PRODUCTS:
      return {
        ...state,
        loading: true,
        error: false,
      };

    case actionTypes.REQUEST_PRODUCTS_SUCCESS:
      return {
        ...state,
        products: action.products,
        loading: false,
        error: false,
      };

    case actionTypes.REQUEST_PRODUCTS_FAILED:
      return {
        ...state,
        loading: false,
        error: true,
      };

    default:
      return state;
  }
}
