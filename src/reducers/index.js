import { combineReducers } from "redux";

import { connectRouter } from "connected-react-router";
// import dataReducer from "./challengeReducer";
import productsReducer from "./productsReducer";
import productsComparisonReducer from "./productsComparisonReducer";

const rootReducer = (history) =>
  combineReducers({
    router: connectRouter(history),
    // dataReducer,
    productsReducer,
    productsComparisonReducer,
  });

export default rootReducer;
