import * as actionTypes from "../actions/productsComparisionActions";
import * as actionType from "../actions/productsActions";
import { LOCATION_CHANGE } from "connected-react-router";

const initialState = {
  productList: [],
  loading: false,
  featureList: null,
  compareData: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      if (action.payload.location.pathname !== "/transactions") {
        return { ...state, productList: [], compareData: [] };
      } else {
        return {
          ...state,
        };
      }

    case actionTypes.ADD_PRODUCT:
      const productid = action.product.productid;

      const data = state.featureList.map((feature) => {
        const obj = {};

        obj["title"] = feature.title;
        obj["features"] = [];
        obj["productid"] = productid;

        feature.features.map((featureData) => {
          const featureObj = {};
          featureObj["featureName"] = featureData.featureName;
          featureObj["value"] = featureData.values[productid];

          obj["features"].push(featureObj);
        });

        return obj;
      });

      return {
        ...state,
        loading: false,
        productList: [...state.productList, action.product],
        compareData: [...state.compareData, data],
      };

    case actionTypes.REMOVE_PRODUCT:
      const d = state.compareData.filter((data) => {
        return data[0].productid !== action.product.productid;
        // data.feature;
      });

      return {
        ...state,
        loading: false,
        productList: state.productList.filter((product) => {
          return product.productid !== action.product.productid;
        }),
        compareData: state.compareData.filter((data) => {
          return data[0].productid !== action.product.productid;
        }),
      };

    case actionType.FEATURES_LIST:
      return {
        ...state,
        loading: false,
        featureList: action.featureList,
      };

    default:
      return state;
  }
}
